@extends('master')
@section('title', 'All Data')
@section('content')

<a href="/question/create" class="btn btn-primary">Tambah</a>
        <table class="table">
            <thead class="thead-light">
              <tr>
                <th scope="col">#</th>
                <th scope="col">Title</th>
                <th scope="col">Description</th>
                <th scope="col">Actions</th>
              </tr>
            </thead>
            <tdesc>
                @forelse ($question as $key=>$value)
                    <tr>
                        <td>{{$key + 1}}</th>
                        <td>{{$value->title}}</td>
                        <td>{{$value->desc}}</td>
                        <td>
                            <form action="/question/{{$value->id}}" method="POST">
                            <a href="/question/{{$value->id}}" class="btn btn-info">Show</a>
                            <a href="/question/{{$value->id}}/edit" class="btn btn-primary">Edit</a>
                                @csrf
                                @method('DELETE')
                                <input type="submit" class="btn btn-danger my-1" value="Delete">
                            </form>
                        </td>
                    </tr>
                @empty
                    <tr colspan="3">
                        <td>No data</td>
                    </tr>  
                @endforelse              
            </tdesc>
        </table>
@endsection