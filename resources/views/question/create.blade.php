@extends('master')
@section('title', 'Add Data')
@section('content')
<h2>Tambah Data</h2>
<div>
<form action="/question" method="POST">
    @csrf
    <div class="form-group">
        <label for="title">Title</label>
        <input type="text" class="form-control" name="title" id="title" placeholder="Masukkan Title">
        @error('title')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <div class="form-group">
        <label for="desc">Description</label>
        <textarea class="form-control" name="desc" id="desc" placeholder="Masukkan Description"></textarea>
        @error('desc')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <button type="submit" class="btn btn-primary">Tambah</button>
</form>
</div>

@error('desc')
<div class="alert alert-danger">
{{ $message }}
</div>
@enderror

@endsection