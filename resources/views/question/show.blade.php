@extends('master')
@section('title', 'Detail Data')
@section('content')
    <h2>Show Question {{$question->id}}</h2><hr>

        <h4>{{$question->title}}</h4>
        <p style="border-style: dotted;">{{$question->desc}}</p><br>

    <div>
        <a href="/question" class="btn btn-dark">Back</a>
    </div>
@endsection